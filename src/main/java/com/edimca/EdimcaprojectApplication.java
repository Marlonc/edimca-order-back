package com.edimca;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EdimcaprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(EdimcaprojectApplication.class, args);
	}

}
