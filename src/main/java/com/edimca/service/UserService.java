package com.edimca.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edimca.dao.UserDAO;
import com.edimca.model.User;

@Service
public class UserService {

	@Autowired
	private UserDAO userDAO;

	public User create(User user) {
		return userDAO.save(user);
	}

	public List<User> getAllUsers() {
		return userDAO.findAll();
	}

	public void delete(User user) {
		userDAO.delete(user);
	}

	public Optional<User> findById(Long usrId) {
		return userDAO.findById(usrId);
	}
	
	public Boolean findCredentials(String userName, String password) {
		boolean isLogin = false;
		List<User> _listaUserTmp= userDAO.findByUsrNickNameAndUsrPassword(userName, password);
		if(_listaUserTmp.size()>0) {
			isLogin=true;
		}
		return isLogin;
	}
}
