package com.edimca.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edimca.dao.OrdDetailDAO;
import com.edimca.dao.OrdHeadDAO;
import com.edimca.model.OrdDetail;
import com.edimca.model.OrdHead;


@Service
public class OrdService {

	@Autowired
	private OrdHeadDAO ordHeadDAO;
	
	@Autowired
	private OrdDetailDAO ordDetailDAO;
	
	
	public List<OrdHead> getAllOrd() {
		return ordHeadDAO.findAll();
	}
	
	public OrdHead create(OrdHead ordhead) {
		List<OrdDetail> details = ordhead.getOrdDetailList();
		ordhead.setOrdDetailList(null);
		ordHeadDAO.save(ordhead);
		for(OrdDetail det : details) {
			det.setOrderNo(ordhead.getOrderNo());
		}

		ordDetailDAO.saveAll(details);
		ordhead.setOrdDetailList(details);
		
		return ordhead;
	}
}
