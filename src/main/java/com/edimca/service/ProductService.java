package com.edimca.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edimca.dao.ProductDAO;
import com.edimca.model.Product;

@Service
public class ProductService {
	
	@Autowired
	private ProductDAO productDAO;
	
	public Product create(Product product) {
		return productDAO.save(product);
	}

	public List<Product> getAllUsers() {
		return productDAO.findAll();
	}

	public void deleteById(Long pdtId) {
		productDAO.deleteById(pdtId);
	}

	public Optional<Product> findById(Long usrId) {
		return productDAO.findById(usrId);
	}
	
	public void updateProduct(Product product, Long pdtId) {
		Optional<Product> productFind = findById(pdtId);
		Product productOne = productFind.get();
		productOne.setPdtId(product.getPdtId());
		productOne.setPdtName(product.getPdtName());
		productOne.setPdtPrice(product.getPdtPrice());
		productOne.setPdtCreate(product.getPdtCreate());
		productOne.setPdtLastUpdate(product.getPdtLastUpdate());
		productOne.setPdtState(product.getPdtState());
		productDAO.save(productOne);
	}
}
