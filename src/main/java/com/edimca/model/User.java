package com.edimca.model;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class User {

	@Id
	@Column(name = "usr_id")
	private Long usrId;

	@Column(name = "usr_name")
	private String usrName;
	
	@Column(name = "usr_last_name")
	private String usrLastName;
	
	@Column(name = "usr_nick_name")
	private String usrNickName;
	
	@Column(name = "usr_password")
	private String usrPassword;
	
	@Column(name = "usr_create")
	private Date usrCreate;
	
	@Column(name = "usr_last_update")
	private Date usrLastUpdate;
	
	@Column(name = "usr_state")
	private String usrState;

	public Long getUsrId() {
		return usrId;
	}

	public void setUsrId(Long usrId) {
		this.usrId = usrId;
	}

	public String getUsrName() {
		return usrName;
	}

	public void setUsrName(String usrName) {
		this.usrName = usrName;
	}

	public String getUsrLastName() {
		return usrLastName;
	}

	public void setUsrLastName(String usrLastName) {
		this.usrLastName = usrLastName;
	}

	public String getUsrNickName() {
		return usrNickName;
	}

	public void setUsrNickName(String usrNickName) {
		this.usrNickName = usrNickName;
	}

	public String getUsrPassword() {
		return usrPassword;
	}

	public void setUsrPassword(String usrPassword) {
		this.usrPassword = usrPassword;
	}

	public Date getUsrCreate() {
		return usrCreate;
	}

	public void setUsrCreate(Date usrCreate) {
		this.usrCreate = usrCreate;
	}

	public Date getUsrLastUpdate() {
		return usrLastUpdate;
	}

	public void setUsrLastUpdate(Date usrLastUpdate) {
		this.usrLastUpdate = usrLastUpdate;
	}

	public String getUsrState() {
		return usrState;
	}

	public void setUsrState(String usrState) {
		this.usrState = usrState;
	}
	
}
