package com.edimca.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ordhead")
public class OrdHead implements Serializable{


	private static final long serialVersionUID = 3304481877329748216L;

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)  
	@Basic(optional = false)
	@Column(name = "order_no")
	private Long orderNo;
	
	@Column(name = "cedula_cli")
	private String cedulaCli;
	
	@Column(name = "order_create")
	private Date orderCreate;
	
	@Column(name = "order_price_total")
	private Double orderPriceTotal;
	
	@Column(name = "name_cli")
	private String nameCli;
	
	@OneToMany(mappedBy = "orderNo")
	private List<OrdDetail> ordDetailList;

	
	public String getCedulaCli() {
		return cedulaCli;
	}

	public void setCedulaCli(String cedulaCli) {
		this.cedulaCli = cedulaCli;
	}

	public String getNameCli() {
		return nameCli;
	}

	public void setNameCli(String nameCli) {
		this.nameCli = nameCli;
	}

	public Long getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(Long orderNo) {
		this.orderNo = orderNo;
	}

	public Date getOrderCreate() {
		return orderCreate;
	}

	public void setOrderCreate(Date orderCreate) {
		this.orderCreate = orderCreate;
	}

	public Double getOrderPriceTotal() {
		return orderPriceTotal;
	}

	public void setOrderPriceTotal(Double orderPriceTotal) {
		this.orderPriceTotal = orderPriceTotal;
	}

	public List<OrdDetail> getOrdDetailList() {
		return ordDetailList;
	}

	public void setOrdDetailList(List<OrdDetail> ordDetailList) {
		this.ordDetailList = ordDetailList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
