package com.edimca.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "orddetail")
public class OrdDetail implements Serializable{

	private static final long serialVersionUID = -4312872079378034890L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)  
	@Basic(optional = false)
	@Column(name = "id_Detalle")
	private Long idDetail;
	

	@Column(name = "pdt_id")
	private Long pdtId;

	@Column(name = "quantity")
	private Double quantity;
	
	@Column(name = "unit_cost")
	private Double unitCost;
	
	@Column(name = "price")
	private Double price;
	
	@Column(name = "order_no")
	private Long orderNo;

	public Long getIdDetail() {
		return idDetail;
	}

	public void setIdDetail(Long idDetail) {
		this.idDetail = idDetail;
	}

	public Long getPdtId() {
		return pdtId;
	}

	public void setPdtId(Long pdtId) {
		this.pdtId = pdtId;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getUnitCost() {
		return unitCost;
	}

	public void setUnitCost(Double unitCost) {
		this.unitCost = unitCost;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Long getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(Long orderNo) {
		this.orderNo = orderNo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
