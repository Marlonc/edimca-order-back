package com.edimca.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "product")
@Embeddable
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)  
	@Column(name = "pdt_id")
	private Long pdtId;

	@Column(name = "pdt_name")
	private String pdtName;

	@Column(name = "pdt_price")
	private Double pdtPrice;

	@Column(name = "pdt_create")
	private Date pdtCreate;

	@Column(name = "pdt_last_update")
	private Date pdtLastUpdate;

	@Column(name = "pdt_state")
	private String pdtState;

	public Long getPdtId() {
		return pdtId;
	}

	public void setPdtId(Long pdtId) {
		this.pdtId = pdtId;
	}

	public String getPdtName() {
		return pdtName;
	}

	public void setPdtName(String pdtName) {
		this.pdtName = pdtName;
	}

	public Double getPdtPrice() {
		return pdtPrice;
	}

	public void setPdtPrice(Double pdtPrice) {
		this.pdtPrice = pdtPrice;
	}

	public Date getPdtCreate() {
		return pdtCreate;
	}

	public void setPdtCreate(Date pdtCreate) {
		this.pdtCreate = pdtCreate;
	}

	public Date getPdtLastUpdate() {
		return pdtLastUpdate;
	}

	public void setPdtLastUpdate(Date pdtLastUpdate) {
		this.pdtLastUpdate = pdtLastUpdate;
	}

	public String getPdtState() {
		return pdtState;
	}

	public void setPdtState(String pdtState) {
		this.pdtState = pdtState;
	}

}
