package com.edimca.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.edimca.model.User;

public interface UserDAO extends JpaRepository<User, Long>{
	
	List<User> findByUsrNickNameAndUsrPassword(String usrNickName, String Password);
}
