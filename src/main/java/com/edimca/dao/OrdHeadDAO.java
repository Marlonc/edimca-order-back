package com.edimca.dao;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.edimca.model.OrdHead;

public interface OrdHeadDAO extends JpaRepository<OrdHead, Serializable>{

}
