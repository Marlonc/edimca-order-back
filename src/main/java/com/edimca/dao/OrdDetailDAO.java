package com.edimca.dao;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.edimca.model.OrdDetail;

public interface OrdDetailDAO extends JpaRepository<OrdDetail, Serializable>{

}
