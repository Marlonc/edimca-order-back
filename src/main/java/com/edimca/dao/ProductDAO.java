package com.edimca.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.edimca.model.Product;

public interface ProductDAO extends JpaRepository<Product, Long>{
}
