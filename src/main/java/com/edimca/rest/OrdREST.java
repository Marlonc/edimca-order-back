package com.edimca.rest;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.edimca.model.OrdHead;
import com.edimca.service.OrdService;

@RestController
@RequestMapping("ord")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class OrdREST {
	
	@Autowired
	private OrdService ordService;
	
	@GetMapping("/listAllOrd")
	private ResponseEntity<List<OrdHead>> listAllOrd() {
		return ResponseEntity.ok(ordService.getAllOrd());
	}
	
	@PostMapping("/saveOrd")
	private ResponseEntity<OrdHead> save(@RequestBody OrdHead ordHead) {
		OrdHead ordTemporality = ordService.create(ordHead);
		try {
			return ResponseEntity.created(new URI("/saveOrd" + ordTemporality.getOrderNo()))
					.body(ordTemporality);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}

}
