package com.edimca.rest;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;

import com.edimca.model.Product;
import com.edimca.service.ProductService;

@RestController
@RequestMapping("product")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE, RequestMethod.PUT})
public class ProductREST {
	
	
	@Autowired
	private ProductService productService;
	
	@GetMapping("/listAllProduct")
	private ResponseEntity<List<Product>> listAllProduct() {
		return ResponseEntity.ok(productService.getAllUsers());
	}
	
	@PostMapping("/saveProduct")
	private ResponseEntity<Product> save(@RequestBody Product product) {
		Product productTemporality = productService.create(product);
		try {
			return ResponseEntity.created(new URI("/saveProduct" + productTemporality.getPdtId()))
					.body(productTemporality);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}
	
	@DeleteMapping(value = "deleteById/{pdtId}")
	private ResponseEntity<Void> deleteUser(@PathVariable("pdtId") Long pdtId) {
		productService.deleteById(pdtId);
		return ResponseEntity.ok().build();
	}
	
	@PutMapping(value = "updateProduct/{pdtId}")
	private ResponseEntity<Void> updateProduct(@RequestBody Product product, @PathVariable("pdtId") Long pdtId) {
		productService.updateProduct(product, pdtId);
		return ResponseEntity.ok().build();
	}
	

}
