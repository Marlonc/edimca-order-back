package com.edimca.rest;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;

import com.edimca.model.User;
import com.edimca.service.UserService;

@RestController
@RequestMapping("user")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET})
public class UserREST {

	@Autowired
	private UserService userService;


	@GetMapping("/listAllUser")
	private ResponseEntity<List<User>> listAllUser() {
		return ResponseEntity.ok(userService.getAllUsers());
	}
	
	@GetMapping(value = "findCredentials/{nickName}/{password}")
	private boolean UserById(@PathVariable("nickName") String nickName, @PathVariable("password") String password) {
		boolean isLogin = userService.findCredentials(nickName, password);
		return isLogin;
	}
	
	/*@DeleteMapping
	private ResponseEntity<Void> deleteUser(@RequestBody User user) {
		userService.delete(user);
		return ResponseEntity.ok().build();
	}*/

	/*@GetMapping(value = "findUser/{id}")
	private ResponseEntity<Optional<User>> listUserById(@PathVariable("id") Long usr_id) {
		return ResponseEntity.ok(userService.findById(usr_id));
	}*/
	
	/*@PostMapping("/api/user/save")
	private ResponseEntity<User> save(@RequestBody User user) {
		User userTemporality = userService.create(user);
		try {
			return ResponseEntity.created(new URI("/api/user/save" + userTemporality.getUsrId()))
					.body(userTemporality);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}*/
}
